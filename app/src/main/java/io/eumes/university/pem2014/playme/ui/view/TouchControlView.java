package io.eumes.university.pem2014.playme.ui.view;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.lang.ref.WeakReference;

public class TouchControlView extends SurfaceView implements SurfaceHolder.Callback{

    public interface Callback{

        public void left();
        public void right();

        public void up();
        public void down();

        public void stop();

    }

    private final static String TAG = TouchControlView.class.getSimpleName();

    private boolean holderInitialized;
    private SurfaceHolder surfaceHolder;

    private WeakReference<Callback> callback;

    public TouchControlView(Context context) {
        super(context);
        initializeHolder();
    }

    public TouchControlView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeHolder();
    }

    public TouchControlView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initializeHolder();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        initializeContext(surfaceHolder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) { }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) { }

    private void initializeContext(SurfaceHolder holder){
        surfaceHolder = holder;
    }

    public void setCallback(Callback callback){
        this.callback = new WeakReference(callback);
    }

    private void initializeHolder(){
        if (holderInitialized)
            return;

        holderInitialized = true;

        //get a reference to the surface holder and attach callback
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

        //enable touch input
        setFocusable(true);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {


        if (event.getAction() == MotionEvent.ACTION_UP){
            if (callback != null && callback.get() != null)
                callback.get().stop();

            Log.i(TAG, "touch up -> STOP");
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN){

            int horizontalSplit = getWidth() / 2;
            int touchPointX = (int)event.getX();

            if (touchPointX <= horizontalSplit) {
                if (callback != null && callback.get() != null)
                    callback.get().left();

                Log.i(TAG, "touch down -> LEFT");
            }
            else {
                if (callback != null && callback.get() != null)
                    callback.get().right();

                Log.i(TAG, "touch down -> RIGHT");
            }

        }


        //double buffered canvas manipulation
        Canvas canvas = surfaceHolder.lockCanvas();

        Paint background = new Paint();
        background.setColor(Color.BLACK);
        background.setStyle(Paint.Style.FILL);

        Paint highlight = new Paint();
        highlight.setColor(Color.WHITE);
        highlight.setStyle(Paint.Style.FILL);

        //clear the complete canvas
        canvas.drawRect(new Rect(0, 0, getWidth(), getHeight()), background);

        //and draw a circle if its a touch down
        if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE){
            canvas.drawCircle(event.getX(), event.getY(), 50, highlight);
        }

        //release the buffered canvas and trigger redraw
        surfaceHolder.unlockCanvasAndPost(canvas);

        //we handled the event
        return true;
    }

}
