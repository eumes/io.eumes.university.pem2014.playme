package io.eumes.university.pem2014.playme.util;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Ticker {

    public interface Callback{

        public void tick();

    }

    private WeakReference<Callback> callback;
    private ScheduledExecutorService scheduler;

    public Ticker(Callback tickCallback){
        callback = new WeakReference<>(tickCallback);
    }

    public void start(){
        if (scheduler != null)
            scheduler.shutdownNow();

        scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleWithFixedDelay(triggerTickerCallback, 0, 20, TimeUnit.MICROSECONDS);
    }

    public void stop(){
        scheduler.shutdown();
    }

    private Runnable triggerTickerCallback = new Runnable() {
        @Override
        public void run() {

            if (callback == null)
                return;

            if (callback.get() == null)
                return;

            callback.get().tick();
        }
    };





}
