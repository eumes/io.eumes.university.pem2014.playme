package io.eumes.university.pem2014.playme.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import io.eumes.university.pem2014.playme.R;

public class BackgroundScene extends Sprite{

    private static final int MOVE_BY_PIXEL = 15;

    private long framePeriod;
    private long frameTime;

    public BackgroundScene(Context context, int width, int height){
        super(context);

        availableCanvasWidth = width;
        availableCanvasHeight = height;

        sprite = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);

        sourceRect = new Rect(0, 0, sprite.getWidth(), sprite.getHeight());

        int left = (width - sprite.getWidth()) / 2;
        int top = height - sprite.getHeight();
        int right = left + sprite.getWidth();
        int bottom = top + sprite.getHeight();
        destinationRect = new Rect(left, top, right, bottom);

        framePeriod = 100;
        frameTime = 0;
    }

    private void updateSourceRectangles(){

        if (direction == Direction.STOP)
            return;

        int moveBy = 0;
        if (direction == Direction.RIGHT) {
            moveBy = -MOVE_BY_PIXEL;
            if (destinationRect.right + moveBy < availableCanvasWidth)
                return;
        }

        if (direction == Direction.LEFT){
            moveBy = MOVE_BY_PIXEL;
            if (destinationRect.left + moveBy > 0)
                return;
        }

        destinationRect.left = destinationRect.left + moveBy;
        destinationRect.right =  destinationRect.right + moveBy;
    }

    @Override
    public void update(long gameTime) {

        if (gameTime <= (frameTime + framePeriod))
            return;

        frameTime = gameTime;

        //update animation
        updateSourceRectangles();

    }


}
