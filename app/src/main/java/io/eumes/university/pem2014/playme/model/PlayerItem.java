package io.eumes.university.pem2014.playme.model;


import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import io.eumes.university.pem2014.playme.R;

public class PlayerItem extends Sprite {

    private static final int NUMBER_OF_SPRITES_FOR_ANIMATION = 6;

    private long framePeriod;
    private long frameTime;

    private int spriteIndex;
    private int singleSpriteWidth;

    public PlayerItem(Context context, int width, int height){
        super(context);

        availableCanvasWidth = width;
        availableCanvasHeight = height;

        framePeriod = 100;
        frameTime = 0;

        setupSpriteForDirection();
    }

    private void updateSourceRectangles(){
        sourceRect.left = spriteIndex * singleSpriteWidth;
        sourceRect.right = sourceRect.left + singleSpriteWidth;
    }

    private void calculateNextSpriteIndex(){
        if (direction == Direction.STOP)
            return;

        spriteIndex = (spriteIndex + 1) % NUMBER_OF_SPRITES_FOR_ANIMATION;
    }

    private void setupSpriteForDirection(){

        switch (direction){
            case LEFT:
                sprite = BitmapFactory.decodeResource(context.getResources(), R.drawable.character_animated_left);
                singleSpriteWidth = sprite.getWidth() / NUMBER_OF_SPRITES_FOR_ANIMATION;
                break;

            case RIGHT:
                sprite = BitmapFactory.decodeResource(context.getResources(), R.drawable.character_animated_right);
                singleSpriteWidth = sprite.getWidth() / NUMBER_OF_SPRITES_FOR_ANIMATION;
                break;

            case STOP:
                sprite = BitmapFactory.decodeResource(context.getResources(), R.drawable.character_still);
                singleSpriteWidth = sprite.getWidth();
                break;
        }

        spriteIndex = 0;
        sourceRect = new Rect(0, 0, singleSpriteWidth, sprite.getHeight());

        int left = (availableCanvasWidth - singleSpriteWidth) / 2;
        int top = availableCanvasHeight - sprite.getHeight();
        int right = left + singleSpriteWidth;
        int bottom = top + sprite.getHeight();
        destinationRect = new Rect(left, top, right, bottom);

    }

    @Override
    public void setDirection(Direction direction) {
        super.setDirection(direction);

        setupSpriteForDirection();
    }

    @Override
    public void update(long gameTime) {
        if (gameTime <= (frameTime + framePeriod))
            return;

        frameTime = gameTime;

        //update animation
        calculateNextSpriteIndex();
        updateSourceRectangles();
    }
}
