package io.eumes.university.pem2014.playme.ui.activity;

import android.app.Activity;
import android.os.Bundle;

import io.eumes.university.pem2014.playme.R;
import io.eumes.university.pem2014.playme.model.Sprite;
import io.eumes.university.pem2014.playme.ui.view.GameBoxView;
import io.eumes.university.pem2014.playme.ui.view.TouchControlView;

public class GameActivity extends Activity {


    private GameBoxView gameBox;
    private TouchControlView touchControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_window);

        //wire up the views
        gameBox = (GameBoxView)findViewById(R.id.gameBox);
        touchControl = (TouchControlView)findViewById(R.id.touchControl);

        //add a touch callback from the touch control
        touchControl.setCallback(touchCallback);
    }

    private TouchControlView.Callback touchCallback = new TouchControlView.Callback() {
        @Override
        public void left() {
            gameBox.setDirection(Sprite.Direction.LEFT);
        }

        @Override
        public void right() {
            gameBox.setDirection(Sprite.Direction.RIGHT);
        }

        @Override
        public void up() { }

        @Override
        public void down() { }

        @Override
        public void stop() {
            gameBox.setDirection(Sprite.Direction.STOP);
        }
    };
}
