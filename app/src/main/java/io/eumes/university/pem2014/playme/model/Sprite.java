package io.eumes.university.pem2014.playme.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

public abstract class Sprite {

    public enum Direction{
        LEFT, RIGHT, STOP
    }

    protected Rect sourceRect;
    protected Rect destinationRect;

    protected int availableCanvasWidth;
    protected int availableCanvasHeight;

    protected Bitmap sprite;

    protected Context context;

    protected Direction direction;

    public Sprite(Context ctx){
        context = ctx;
        sprite = null;
        availableCanvasWidth = 0;
        availableCanvasHeight = 0;
        sourceRect = new Rect(0, 0, 0, 0);
        destinationRect = new Rect(0, 0, 0, 0);
        direction = Direction.STOP;
    }

    public abstract void update(long gameTime);

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public void draw(Canvas canvas){
        if (sprite == null)
            return;

        canvas.drawBitmap(sprite, sourceRect, destinationRect, null);
    }

}
