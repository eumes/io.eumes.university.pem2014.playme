package io.eumes.university.pem2014.playme.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import io.eumes.university.pem2014.playme.model.BackgroundScene;
import io.eumes.university.pem2014.playme.model.PlayerItem;
import io.eumes.university.pem2014.playme.model.Sprite;
import io.eumes.university.pem2014.playme.util.Ticker;

public class GameBoxView extends SurfaceView implements SurfaceHolder.Callback, Ticker.Callback{

    private final static String TAG = GameBoxView.class.getSimpleName();

    private boolean holderInitialized = false;

    private Ticker ticker;
    private BackgroundScene background;
    private PlayerItem player;
    private SurfaceHolder surfaceHolder;

    public GameBoxView(Context context) {
        super(context);
        initializeHolder();
    }

    public GameBoxView(Context context, AttributeSet attrs){
        super(context, attrs);
        initializeHolder();
    }
    public GameBoxView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        initializeHolder();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        initializeContext(surfaceHolder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {}

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        shutdownContext();
    }

    private void initializeHolder(){
        if (holderInitialized)
            return;

        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        holderInitialized = true;
    }

    private void initializeContext(SurfaceHolder holder){

        //initialize the items on the canvas
        background = new BackgroundScene(getContext(), getWidth(), getHeight());
        player = new PlayerItem(getContext(), getWidth(), getHeight());

        //start the ticker for redraws
        ticker = new Ticker(this);
        ticker.start();

        Log.i(TAG, "initialized context, ticker started");
    }

    private void shutdownContext(){
        if (ticker == null)
            return;

        //stop the ticker
        ticker.stop();
    }

    @Override
    public void tick() {

        //we got a new ticker from the timer, animate the scene
        long time = System.currentTimeMillis();

        //have the items update themselves
        background.update(time);
        player.update(time);

        //double buffered canvas manipulation
        Canvas canvas = surfaceHolder.lockCanvas();

        //draw the items
        background.draw(canvas);
        player.draw(canvas);

        //and release the canvas again, also triggering a redraw
        surfaceHolder.unlockCanvasAndPost(canvas);
    }

    public void setDirection(Sprite.Direction direction){
        //change the direction we are currently in
        background.setDirection(direction);
        player.setDirection(direction);
    }


}
